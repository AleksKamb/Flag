module.exports = function (grunt) {
	grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
            sass: {
              dist: {
                files: {
                  "css/style.css": "sass/style.scss"
                },
                options: {
                  sourcemap: false
                }
              }
            },
            exec: {
              echo_something: 'rollup -c'
            },
            watch: {
              css: {
                files: "**/*.scss",
                tasks: ["sass"]
              },
              js: {
                files: "js/script.js",
                tasks: ["exec"]
              }
            }
          });
    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-exec");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.registerTask("default", ["watch"]);
};
