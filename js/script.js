         AOS.init();

        function map() {
            var mymap = L.map('map').setView([51.505, -0.09], 13);
             var marker = L.marker([51.505, -0.09], {draggable:true}).addTo(mymap);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYWxla3NrYW1iIiwiYSI6ImNqanEwOWJjdzJxYXgzcW1kNHB5eWFyZmwifQ.CIy-TzzU0qjU7si8OQTkRA', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'pk.eyJ1IjoiYWxla3NrYW1iIiwiYSI6ImNqanEwOWJjdzJxYXgzcW1kNHB5eWFyZmwifQ.CIy-TzzU0qjU7si8OQTkRA',

            }).addTo(mymap);

        }

        map();


        function collapse(x) {
            x.classList.toggle("collapsed");
        }
        

        $(document).ready(function () {
            
            //fixes Chrome bug where video doesn't loop
            setInterval(function() {
                $('.background').attr('src', 'video.mp4').load();
            }, 42000);
           
          

            $(".menu-collapse").click(function () {
                $(".mobile-menu").slideToggle("slow")
            });


            // page scroll script 
            $(".about-btn").click(function () {
                $('html,body').animate({
                    scrollTop: $("#about").offset().top
                },
                    'slow');
            });
            $(".menu-btn").click(function () {
                $('html,body').animate({
                    scrollTop: $("#menu").offset().top
                },
                    'slow');
            });
            $(".book-btn").click(function () {
                $('html,body').animate({
                    scrollTop: $("#book").offset().top
                },
                    'slow');
            });
            
        });



